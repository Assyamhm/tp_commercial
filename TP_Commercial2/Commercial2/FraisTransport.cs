﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace Commercial2
{
    [System.Serializable]
    public class FraisTransport : NoteFrais
    {
        private double km;
        public FraisTransport(DateTime date, Commercial leCommercial, int km) : base(date, leCommercial)
        {
            this.km = km;
            this.SetMontantARembourser();
        }
        public override double CalculMontantARembourser()
        {
            double montantARembourser = 0;
            if (this.LeCommercial.Puissance < 5)
            {
                montantARembourser = 0.1 * km;
            }
            else if (this.LeCommercial.Puissance <= 10)
            {
                montantARembourser = 0.2 * km;
            }
            else
            {
                montantARembourser = 0.3 * km;
            }
            return montantARembourser;
        }

    }
}