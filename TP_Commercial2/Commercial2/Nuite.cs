﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Commercial2
{
    [System.Serializable]
    public class Nuite : NoteFrais
    {
        private double region;
        private double montant;

        public Nuite(DateTime date, Commercial leCommercial, char region, double montant) : base(date, leCommercial)
        {
            this.region = region;
            this.montant = montant;
            this.SetMontantARembourser();

        }
        public override double CalculMontantARembourser()
        {
            double montantARembourser = 0;
            if (this.LeCommercial.Bareme == 'A')
            {
                montantARembourser = 65;
             
            }
            else if (this.LeCommercial.Bareme == 'B')
            {
                montantARembourser = 55;
               
            }
            else
            {
                montantARembourser = 50;
            }
            if (this.region == 1)
            {
                montantARembourser = 0.9 * montantARembourser;
            }
            else if (this.region == 2)
            {
                montantARembourser = 1 * montantARembourser;
            }
            else
            {
                montantARembourser = 1.15 * montantARembourser;
            }

            return montantARembourser;
        }
    }
}
