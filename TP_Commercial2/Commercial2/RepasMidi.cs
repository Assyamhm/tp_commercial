﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Commercial2
{
    [System.Serializable]
    public class RepasMidi : NoteFrais
    {
        private double facture;

        public RepasMidi(DateTime date, Commercial leCommercial, double facture) : base(date, leCommercial)
        {
            this.facture = facture;
            this.SetMontantARembourser();

        }
        public override double CalculMontantARembourser()
        {
            double montantARembourser = 0;
            if (this.LeCommercial.Bareme == 'A')
            {
                montantARembourser = 0.1 * 25;
            }
            else if (this.LeCommercial.Bareme == 'B')
            {
                montantARembourser = 0.2 * 22;
            }
            else
            {
                montantARembourser = 0.3 * 20;
            }
            return montantARembourser;
        }

    }
}

