﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Commercial2
{
    [System.Serializable]
    public class NoteFrais
    {
        private int numero;
        private DateTime date;
        private Commercial leCommercial;
        private double montantARembourser;
        private bool rembourse;
        
        public NoteFrais(DateTime date, Commercial leCommercial)
        {
            this.numero = leCommercial.GetNoteFrais().Count +1;
            this.date = date;
            this.leCommercial = leCommercial;
            this.montantARembourser = 0;
            this.rembourse = false;
            leCommercial.AjouterNoteFrais(this);
        }
        
        public bool Rembourse
        {
            get { return this.rembourse; }
        }
        public DateTime Date
        {
            get { return date; }
        }
        public Commercial LeCommercial
        {
            get { return leCommercial; }
        }
        public virtual void SetMontantARembourser()
        {
            this.montantARembourser = CalculMontantARembourser();
        }
        public double GetMontantARembourser()
        {

            return this.montantARembourser;
        }
        public bool SetRembourse()
        {
            this.rembourse = false;
            if (this.montantARembourser == 0)
            {
                this.rembourse = true;
            }
            return this.rembourse;
        }

        virtual public double CalculMontantARembourser()
        {
            return 0;
        }

        public override string ToString()
        {
            string m = "Non remboursé";
            if (rembourse == true)
            {
                m = "Remboursé";
            }
            string message = "Numéro : 1 - Date : " + this.date + " - montant à rembourser : " + this.montantARembourser + " euros - " + m;
            return message;
        }
    }
}