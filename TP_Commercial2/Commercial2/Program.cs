﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Commercial2
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCommercial sc;
            Commercial c1;
            sc = new ServiceCommercial();
            c1 = new Commercial("Dupond", "Jean", 'B', 7);

            c1.AjouterNoteFrais( new DateTime(2013, 11, 15), 100); // ajoute un frais de transport
            c1.AjouterNoteFrais(new DateTime(2013, 11, 21), 15.5); // ajoute une note de repas
            c1.AjouterNoteFrais( new DateTime(2013, 11, 25),'2', 105); // ajoute une nuité
            sc.AjouterCommercial(c1);
            PersisteCommercial.Sauver("service.sr", sc);
            ServiceCommercial sc1 = PersisteCommercial.Charger("service.sr");
        }
    }
}
