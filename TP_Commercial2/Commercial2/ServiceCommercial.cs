﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Commercial2
{
    [System.Serializable]
    public class ServiceCommercial
    {
        private List<Commercial> lesCommerciaux;

        public ServiceCommercial()
        {
            this.lesCommerciaux = new List <Commercial>();
        }

        public List<Commercial> LesCommerciaux
        {
            get { return lesCommerciaux; }
            set { lesCommerciaux = value; }
        }

        public void AjouterCommercial(Commercial c)
        {
            this.lesCommerciaux.Add(c);
        }
        public int NbFraisNonRembourses()
        {
            int fraisNonRembourses = 0;
            foreach (Commercial c in this.lesCommerciaux)
            {
                foreach (NoteFrais n in c.LesFrais)
                {
                    if (n.Rembourse == false)
                    {
                        fraisNonRembourses += 1;
                    }
                }
            }
            return fraisNonRembourses;
        }
    }
}
