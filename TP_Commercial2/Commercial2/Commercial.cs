﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Commercial2
{
    [System.Serializable]
    public class Commercial
    {
        private string nom;
        private string prenom;
        private char bareme;
        private int puissance;
        private List<NoteFrais> lesFrais;
        public Commercial(string nom, string prenom, char bareme, int puissance)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.bareme = bareme;
            this.puissance = puissance;
            this.lesFrais = new List<NoteFrais>();
        }

        public string Prenom
        {
            get { return this.prenom; }
        }
        public char Bareme
        {
            get { return this.bareme; }
        }
        public string Nom
        {
            get { return this.nom; }
        }
        public int Puissance
        {
            get { return this.puissance; }
        }
        
        public List<NoteFrais> LesFrais
        {
            get { return this.lesFrais; }
        }
        public void AjouterNoteFrais(NoteFrais f)
        {
            this.lesFrais.Add(f);
        }
        public void AjouterNoteFrais(DateTime date, int km)
        {
            FraisTransport nT = new FraisTransport(date, this, km);
            lesFrais.Add(nT);
        }
        public void AjouterNoteFrais(DateTime date, double facture)
        {
            RepasMidi rM = new RepasMidi(date, this, facture);
            lesFrais.Add(rM);
        }
        public void AjouterNoteFrais(DateTime date, char region, double montant)
        {
            Nuite n = new Nuite(date, this, region, montant);
            lesFrais.Add(n);
        }
        public List<NoteFrais> GetNoteFrais()
        {

            return this.lesFrais;
        }
        public override string ToString()
        {
            string message = "Nom : " + this.nom + " Prenom : " + this.prenom + " Puissance voiture : " + this.puissance + " Categorie : " + this.bareme + ".";
            return message;
        }
    }
}

