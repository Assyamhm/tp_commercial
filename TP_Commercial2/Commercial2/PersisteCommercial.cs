﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Commercial2
{
    class PersisteCommercial
    {

        public static void Sauver(string nomFichier, ServiceCommercial sC)
        {
            FileStream fichier = new FileStream(nomFichier + ".sr", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fichier, sC);
            fichier.Close();
        }
        public static ServiceCommercial  Charger(string nomFichier)
        {
            FileStream fichier = new FileStream(nomFichier + ".sr", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            ServiceCommercial sC = (ServiceCommercial)bf.Deserialize(fichier);
            fichier.Close();
            return sC;
        }
    }
}
