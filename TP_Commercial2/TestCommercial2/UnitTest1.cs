using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commercial2;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TestCommercial2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
       
        public void TestAjouterNoteFrais()
        {
            Commercial c = new Commercial("Jean", "Dupont", 'A', 25);
            NoteFrais f, f1;
            f = new NoteFrais(new DateTime(2013, 11, 12), c);
            f1 = new NoteFrais(new DateTime(2013, 11, 15), c);
            Assert.AreEqual(2, c.GetNoteFrais().Count);
        }
    }
}
